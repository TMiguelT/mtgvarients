package com.miguel.mtgvarients;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.io.IOException;
import java.util.Random;

/**
 * Created by Miguel on 11/21/13.
 */
public class PlanechaseFragment extends android.support.v4.app.Fragment
{
    public PlanechaseFragment()
    {

    }

    Random generator;
    String[] planes;
    String last = "";
    String current = "";
    AssetManager assets;
    ImageButton button;

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("pc_current", current);
        outState.putString("pc_last", current);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.planechase_layout, container, false);

        //Setup button
        button = (ImageButton) view.findViewById(R.id.image_planechase);
        button.setOnClickListener(planeClick);

        //Set other variables
        generator = new Random();
        assets = getActivity().getAssets();
        try
        {
            planes = assets.list("planes");
        }
        catch (Exception ex)
        {
        }
        return view;
    }

    private View.OnClickListener planeClick = new View.OnClickListener()
    {
        public void onClick(View v)
        {
            last = current;
            current = planes[generator.nextInt(planes.length)];
            Drawable next = null;
            try
            {
                next = Drawable.createFromStream(assets.open("planes/" + current), null);
            }
            catch (IOException ex)
            {
            }
            button.setImageDrawable(next);
        }
    };
}
