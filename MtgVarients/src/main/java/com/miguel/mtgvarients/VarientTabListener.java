package com.miguel.mtgvarients;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

/**
 * Created by Miguel on 11/21/13.
 */
public class VarientTabListener implements ActionBar.TabListener {
    Fragment Arch_Fragment;
    Fragment Plane_Fragment;
    Activity parent;

    public VarientTabListener(Activity act) {
        parent = act;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        // Check if the fragment is already initialized
        if (tab.getText() == "Archenemy") {
            if (Arch_Fragment == null) {
                // If not, instantiate and add it to the activity
                Arch_Fragment = Fragment.instantiate(parent, ArchenemyFragment.class.getName());
                ft.add(android.R.id.content, Arch_Fragment, "Archenemy");
            } else {
                // If it exists, simply attach it in order to show it
                ft.show(Arch_Fragment);
            }
        } else if (tab.getText() == "Planechase") {
            if (Plane_Fragment == null) {
                // If not, instantiate and add it to the activity
                Plane_Fragment = Fragment.instantiate(parent,PlanechaseFragment.class.getName());
                ft.add(android.R.id.content, Plane_Fragment, "Planechase");
            } else {
                // If it exists, simply attach it in order to show it
                ft.show(Plane_Fragment);
            }
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        // Check if the fragment is already initialized
        if (tab.getText() == "Archenemy") {
            if (Arch_Fragment != null) {
                // Detach the fragment, because another one is being attached
                ft.hide(Arch_Fragment);
            }

        } else if (tab.getText() == "Planechase") {
            if (Plane_Fragment != null) {
                // Detach the fragment, because another one is being attached
                ft.hide(Plane_Fragment);
            }
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}