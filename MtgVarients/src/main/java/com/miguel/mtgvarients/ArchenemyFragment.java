package com.miguel.mtgvarients;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.io.IOException;
import java.util.Random;

/**
 * Created by Miguel on 11/21/13.
 */
public class ArchenemyFragment extends android.support.v4.app.Fragment{
    Random generator;
    String[] schemes;
    AssetManager assets;
    ImageButton button;

    public ArchenemyFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.archenemy_layout, container, false);

        //Setup button
        button = (ImageButton) view.findViewById(R.id.image_archenemy);
        button.setOnClickListener(archClick);

        //Set other variables
        generator = new Random();
        assets = getActivity().getAssets();
        try {
            schemes = assets.list("schemes");
        } catch (Exception ex) {
        }
        return view;
    }

    private View.OnClickListener archClick = new View.OnClickListener() {
        public void onClick(View v) {
            String path = schemes[generator.nextInt(schemes.length)];
            Drawable next = null;
            try {
                next = Drawable.createFromStream(assets.open("schemes/" + path), null);
            } catch (IOException ex) {
            }
            button.setImageDrawable(next);
        }
    };
}
