package com.miguel.mtgvarients;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import java.io.IOException;
import java.util.Random;

public class MainActivity extends ActionBarActivity
{

    String[] Planes;
    String[] Schemes;
    AssetManager assets;

    ImageButton arch;
    ImageButton plane;

    Random generator;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //Auto generated code
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setup action bar
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        //Tabs
        ActionBar.Tab ArchTab = actionBar.newTab()
                .setTabListener(new VarientTabListener(this))
                .setText("Archenemy")
                .setIcon(getResources().getDrawable(R.drawable.archenemy_symbol));
        actionBar.addTab(ArchTab);

        ActionBar.Tab PlaneTab = actionBar.newTab()
                .setTabListener(new VarientTabListener(this))
                .setText("Planechase")
                .setIcon(getResources().getDrawable(R.drawable.planechase_symbol));
        actionBar.addTab(PlaneTab);
    }

    private OnClickListener planeClick = new OnClickListener()
    {
        public void onClick(View v)
        {
            String path = Planes[generator.nextInt(Planes.length)];
            Drawable next = null;
            try
            {
                next = Drawable.createFromStream(assets.open("planes/" + path), null);
            }
            catch (IOException ex)
            {
            }
            arch.setImageDrawable(next);
        }
    };

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event)
//    {
//        if (getFragmentManager().findFragmentById(R.id.fragment_archenemy).isVisible())
//        {
//            ArchenemyFragment f = (ArchenemyFragment)getFragmentManager().findFragmentById(R.id.fragment_archenemy);
//        }
//        else if (getFragmentManager().findFragmentById(R.id.fragment_planechase).isVisible())
//        {
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
